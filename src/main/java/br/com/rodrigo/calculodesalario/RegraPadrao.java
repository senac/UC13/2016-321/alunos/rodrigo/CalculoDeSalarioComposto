/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculodesalario;

/**
 *
 * @author Diamond
 */
public abstract class RegraPadrao implements RegraDeCalculo {

    protected final double limite;
    

    public RegraPadrao(double limite) {
        this.limite = limite;
    }
    
    protected abstract double getTaxaSuperior();
    
    protected abstract double getTaxaInferior();

    @Override
    public double calcula(Funcionario f) {

        if (f.getSalario() > this.limite) {
            return f.getSalario() * this.getTaxaSuperior();
        } else {
            return f.getSalario() * this.getTaxaInferior();
        }

    }

}

