/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculodesalario;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeSalario {
 /*
    As regras de negocio são as seguintes:
    Desenvolvedores possuem 20% de desconto caso seu salario
    seja maior do que R$ 3000,0. Caso contrario, o desconto e de 10%.
    
    ANALISTA MAIS QUE 4000 20% E MENOS 10%
    
    DBAs e testadores possuem desconto de 25% se seus salários forem 
    maiores do que R$ 2500,0. 15%, em caso contrario.
    
    
     */
   

    public double calcular(Funcionario funcionario) {

       return funcionario.getCargo().getRegra().calcula(funcionario);

       
    }   
}
