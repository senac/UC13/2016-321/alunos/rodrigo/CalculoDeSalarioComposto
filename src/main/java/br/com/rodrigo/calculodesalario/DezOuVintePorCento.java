/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculodesalario;

/**
 *
 * @author Diamond
 */

 public class DezOuVintePorCento extends RegraPadrao {

    public DezOuVintePorCento(double limite) {
        super(limite);
    }

    
    
    @Override
    protected double getTaxaSuperior() {
        return 0.80 ; 
    }

    @Override
    protected double getTaxaInferior() {
       return  0.9 ; 
    }   
}

